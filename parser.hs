{-# LANGUAGE TupleSections #-}

import Text.Read

data Expr = Add Expr Expr
          | Mult Expr Expr
          | Lit Int
          | Var String
          deriving Show

type Token = String
type Parser a = [Token] -> Maybe (a, [Token])

-- The reason we tokenize the input before parsing it is (among other things)
-- that we do not want our parser to have to deal with whitespace, but only
-- deal with parts of the input that is meaningful to our parser.
--
-- Preferably we would like a bit more advanced tokenizer, such that e.g.
-- `tokenize "1+23   * 4` returns `["1", "+", "23", "*", "4"]`.
-- Currently this just splits the input by whitespace, so e.g.
-- `parseExpr "1+2"` will fail, while `parseExpr "1 + 2"` will return `Add ...`
-- because the added spaces causes the "correct" tokenization, while the first
-- one will tokenize into `["1+2"]`, and the single token "1+2" does not
-- satisfy any of the parsers (it's not a number).
tokenize :: String -> [Token]
tokenize = words

--------- PRIMITIVE PARSERS (parsers made without using the combinators)

-- Take the first token if it matches the given string
parseString :: String -> Parser String
parseString s ts = case ts of
                         (x:xs) | x == s -> Just (s, xs)
                         _ -> Nothing

-- Try to read the first token as an Int
parseNumber :: Parser Int
parseNumber [] = Nothing
parseNumber (x:xs) = fmap (,xs) $ readMaybe x

--------- COMBINATOR/UTILITY FUNCTIONS

-- Create a parser which always returns `c` and consumes no input.
inject :: a -> Parser a
inject c ts = Just (c, ts)

apply :: Parser (a -> b) -> Parser a -> Parser b
apply pf pa ts = do
    (f, ts') <- pf ts
    (a, ts'') <- pa ts'
    return $ (f a, ts'')

-- Create a parser that runs p until it fails, and then returns the list of
-- all the results. E.g. `parseMany parseNumber ["1", "2", "hello", 3]`
-- should return `Just ([1, 2], ["hello", "3"])`.
parseMany :: Parser a -> Parser [a]
parseMany p = (inject (:) `apply` p `apply` parseMany p) `orElse` inject []

-- Create a new parser which first runs p1, then if p1 fails, runs p2 instead.
orElse :: Parser a -> Parser a -> Parser a
orElse p1 p2 ts = case p1 ts of
                 Nothing -> p2 ts
                 x -> x

-------- PARSERS

parseBool :: Parser Bool
parseBool = parseFalse `orElse` parseTrue
    where parseFalse = p False "false"
          parseTrue = p True "true"
          -- Try to parse string `s`, and if it succeeds, return v instead.
          p v s = inject (const v) `apply` parseString s

parseNumberPair :: Parser (Int, Int)
parseNumberPair = inject (,) `apply` parseNumber `apply` parseNumber

-- Conceptually our grammar looks something like this:
--
-- parseExpr  -> parseAdd
-- parseAdd   -> (parseMult '+' parseAdd) | parseMult
-- parseMult  -> (parseNum  '*' parseNum) | parseNum
-- parseLit   -> (parseDigit parseLit) | parseDigit
-- parseDigit -> '0' | '1' | ... | '9'

parseAdd = inject (foldl Add) `apply` parseMult `apply` parseMany parseTail
    where parseTail = inject (\x y -> y) `apply` parseString "+" `apply` parseMult

parseMult = inject (foldl Mult) `apply` parseLit `apply` parseMany parseTail
    where parseTail = inject (\x y -> y) `apply` parseString "*" `apply` parseLit

parseLit :: Parser Expr
parseLit = inject Lit `apply` parseNumber

parseExpr :: String -> Maybe Expr
parseExpr = fmap fst . parseAdd . tokenize

